﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CarPooling;

namespace CarPoolingWebApp.Models
{
    /// <summary>
    /// This model is especially for the reservation view which contains a few more  
    /// things than other views
    /// </summary>
    public class ViewModelReservation
    {

        private List<Reservation> listReservation;
        /// <summary>
        /// here I keep the dataSource as a class member because 
        /// I'm too lazy to instanciate it anytime I need it. I could have done it in the 
        /// constructor and fetch the lists I need, but these lists might be desynchronized 
        /// from what we actually do have in the DB. Beside the concurrent access it should work.
        /// </summary>
        private IDataSource dataSource;


        public ViewModelReservation()
        {
            listReservation = new List<Reservation>();
            dataSource = CurrentDataSource.CurrentDS;
            listReservation = dataSource.getReservation();

        }

        /// <summary>
        /// This method list all the records from the table reservation 
        /// </summary>
        public List<Reservation> ListReservation
        {
            get
            {
                return listReservation;
            }

            set
            {
                listReservation = value;
            }
        }

        /// <summary>
        /// This method is used to fetch all journeys from the reservation list 
        /// </summary>
        public List<Journey> ListJourney {
            get {
                List<Journey> allJourneys = dataSource.getJourney();
                List<Journey> res = new List<Journey>();
                foreach (Reservation reservation in listReservation) {

                    // first of all does the result already contains anything like that
                    Journey existsInRes = res.Find(x => x.IdJourney.Equals(reservation.RefJourneyId));

                    // no then we are good to go
                    if (null == existsInRes) {

                        // fetch the object that has the very same id
                        Journey fetchedFromDB  = allJourneys.Find(x => x.IdJourney.Equals(reservation.RefJourneyId));

                        // such thing exists... Then add it to the list
                        if (null != fetchedFromDB) { 
                            res.Add(fetchedFromDB);
                        }
                    }
                }

                return res;
            }
        }

        /// <summary>
        /// This method allows you to fetch any participants to a specific journey
        /// </summary>
        /// <param name="pJourneyId"></param>
        /// <returns>A map of the users and his state (null,true,false) from a specific Journey in param</returns>
        public Dictionary<User, Nullable<bool>> ListJourneyUsers(int pJourneyId) {
            Dictionary < User, Nullable < bool >> res  = new Dictionary<User,bool?>();
           
            List<User> registeredUsers = dataSource.getUsers();
            foreach (Reservation reservation in listReservation) {

                // pretty obvious that we are interested only in a certain Journey
                // from the reservations
                if (reservation.RefJourneyId == pJourneyId) {

                    // lambda to filter the object of interest
                    User user = registeredUsers.Find(x => x.IdUser.Equals(reservation.RefUserId));

                    // res do not contain such item then we need to add it
                    if (!res.ContainsKey(user))
                    {
                        res.Add(user, reservation.AcceptedByDriver);
                    }
                }
            }
            return res;
        }
    }
}