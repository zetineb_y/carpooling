﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CarPooling;

namespace CarPoolingWebApp.Models
{
    public class ViewModelUser : User
    {
        private SessionModel currentSession;

        public ViewModelUser()
        {
            currentSession = System.Web.HttpContext.Current.Session["data"] as SessionModel;
            IDataSource dataSource = CurrentDataSource.CurrentDS;
            User UserLogged =  dataSource.getUser(currentSession.UserId);
            Username = UserLogged.Username;
            Name = UserLogged.Name;
            FirstName = UserLogged.FirstName;
            Address = UserLogged.Address;
            City = UserLogged.City;
            DateBirth = UserLogged.DateBirth;
            PostalNumber = UserLogged.PostalNumber;
            PhoneNumber = UserLogged.PhoneNumber;
            CarDescription = UserLogged.CarDescription;
        }
        

    }
}