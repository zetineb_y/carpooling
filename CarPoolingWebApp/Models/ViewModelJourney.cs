﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CarPooling;

namespace CarPoolingWebApp.Models
{
    public class ViewModelJourney
    {

        private List<Journey> listJourney;


        public ViewModelJourney()
        {
            listJourney = new List<Journey>();
            // GestionSource SourceManagement = new GestionSource();
            // IDataSource MaDataSource = SourceManagement.SelectDataSource();
            IDataSource dataSource = CurrentDataSource.CurrentDS;
            listJourney = dataSource.getJourney();

        }

        public List<Journey> ListJourney
        {
            get
            {
                return listJourney;
            }

            set
            {
                listJourney = value;
            }
        }

        public int AcceptedUsers(int pJourneyId)
        {
            IDataSource dataSource = CurrentDataSource.CurrentDS;
            int res = dataSource.getAcceptedUsersNumber(pJourneyId);
            return res;
        }
    }
}