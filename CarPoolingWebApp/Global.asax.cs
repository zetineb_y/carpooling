﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using CarPooling;

namespace CarPoolingWebApp
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            ModelBinders.Binders.Add(typeof(SessionModel), new SessionModelBinder());

            //Define the data source to use. (singleton)
            CurrentDataSource database = CurrentDataSource.Instance();
        }

        void Session_Start(object sender, EventArgs e)
        {
            Session["data"] = new SessionModel();

        }
    }

}
