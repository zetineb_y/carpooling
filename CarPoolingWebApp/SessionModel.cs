﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace CarPoolingWebApp
{
    /// <summary>
    /// Define the session storage values
    /// </summary>
    public class SessionModel
    {
        /// <summary>
        /// Define if the current user is authenticated or not
        /// </summary>
        public Boolean IsAuthentified { get; set; }

        /// <summary>
        /// Define the id in database
        /// </summary>
        public int UserId { get; set; }


        //public String 

        public SessionModel()
        {
            IsAuthentified = false;
            UserId = -1;

        }
    }
}