﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CarPooling;
using CarPoolingWebApp.Models;

namespace CarPoolingWebApp.Controllers
{
    /// <summary>
    /// Controler for the pages carpooling
    /// </summary>
    public class CarPoolingController : UserAwareControllerBase
    {
        /// <summary>
        /// Call the home page
        /// </summary>
        /// <param name="monTrajet"></param>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View(new ViewModelJourney());
        }

        public ActionResult About()
        {

            return View();
        }

        public ActionResult Contact()
        {
            return View();
        }
        /// <summary>
        /// Controler to display connexion page
        /// </summary>
        /// <returns></returns>
        public ActionResult Connexion()
        {
            return View();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pConnexion"></param>
        /// <param name="currentSession"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult SignIn(User pConnexion, SessionModel currentSession)
        {
            IDataSource dataSource = CurrentDataSource.CurrentDS;
            FeedBack report = dataSource.login(pConnexion.Username, pConnexion.Password);

            if (report.Success)
            {
                CarPooling.User infoUser = dataSource.getUser(pConnexion.Username);
                currentSession.IsAuthentified = true;
                currentSession.UserId = infoUser.IdUser;
                return RedirectToAction("Index", "CarPooling");
            }
            else
            {
                return RedirectToAction("LoginError", "CarPooling");
                //currentSession = new SessionModel();
            }
        }


        /// <summary>
        /// This Action allows a user to disconnect from the platform
        /// </summary>
        /// <param name="currentSession"></param>
        /// <returns></returns>
        public ActionResult SignOut(SessionModel currentSession)
        {

            SignOut();
            //Session["data"] = new SessionModel();
            return View("Index", new ViewModelJourney());
        }


        public ActionResult UserProfil()
        {
            return View(new ViewModelUser());
        }

        public ActionResult Register()
        {
            //TODO REGISTER

            return View();
        }

        [HttpPost]
        public ActionResult ActionRegister(User pUser)
        {

            pUser.DateInscription = DateTime.Now;
            IDataSource dataSource = CurrentDataSource.CurrentDS;
            dataSource.addUser(pUser);

            return View("Index", new ViewModelJourney());
        }


        [HttpPost]
        public ActionResult ActionAddJourney(Journey pJourney, SessionModel currentSession)
        {

            pJourney.RefUser = currentSession.UserId;
            IDataSource dataSource = CurrentDataSource.CurrentDS;
            dataSource.addJourney(pJourney);

            return View("Index", new ViewModelJourney());
        }

        [HttpPost]
        public ActionResult ActionModifiedProfil(User pCurrentUser, SessionModel currentSession)
        {
            
            IDataSource dataSource = CurrentDataSource.CurrentDS;
            pCurrentUser.IdUser = currentSession.UserId;
            dataSource.modifyUser(pCurrentUser);

            //TODO 


            return RedirectToAction("UserProfil", "CarPooling");
        }

        [HttpPost]
        public ActionResult ActionParticipateJourney(Journey pJourney, SessionModel currentSession)
        {
            IDataSource dataSource = CurrentDataSource.CurrentDS;
            dataSource.participateJourney(pJourney, currentSession.UserId);
            return RedirectToAction("AllJourney", "CarPooling");
        }

        [HttpPost]
        public ActionResult ActionAcceptUser(int pUserId, int pJourneyId) {
            IDataSource dataSource = CurrentDataSource.CurrentDS;
            dataSource.reservationForUser(pUserId, pJourneyId, true);
            return RedirectToAction("MyReservations", "CarPooling");
        }

        [HttpPost]
        public ActionResult ActionRejectUser(int pUserId, int pJourneyId)
        {
            IDataSource dataSource = CurrentDataSource.CurrentDS;
            dataSource.reservationForUser(pUserId, pJourneyId, false);
            return RedirectToAction("MyReservations", "CarPooling");
        }

        public ActionResult MyReservations() {
           return View(new ViewModelReservation());
        }

        public ActionResult ModifyProfil()
        {
            
            return View(new ViewModelUser());
        }

        public ActionResult AllJourney()
        {

            return View(new ViewModelJourney());
        }

        public ActionResult LoginError()
        {
            return View();
        }

        public ActionResult AddJourney()
        {
            return View();
        }

    }
}