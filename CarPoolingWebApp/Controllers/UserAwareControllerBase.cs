﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CarPoolingWebApp.Controllers
{
    /// <summary>
    /// Controller dedicated to manage user session.
    /// </summary>
    public abstract class UserAwareControllerBase : Controller
    {
        private const string UserSessionKey = "data";

        protected SessionModel UserSession
        {
            get
            {
                return Session[UserSessionKey] as SessionModel;
            }
        }

        protected void SignOut()
        {
            Session.Remove(UserSessionKey);
            Session[UserSessionKey] = new SessionModel();
        }
    }
}