﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarPooling
{
    // delegates allows to evitate to create another class every everytime we want to instantiate a
    // new connexion in a different way

    // a provider which can be configured with delegates
    public class RelayProvider : IProvider
    {
        // Defines delegate
        // delegate = kind of function
        // Func : type with return value
        // Action : no return value

        // connexionProvider --> can be linked to any method which returns a dbEntities

        public Func<DBEntities> ConnectionProvider { get; set; }

        // Call to delegate when the interface defined method is called 

        public DBEntities CreateConnection()
        {
            if (ConnectionProvider != null)
            {
                return ConnectionProvider();
            }
            return null; // method always returns something
        }
    }
}
