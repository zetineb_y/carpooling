
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 06/18/2017 16:46:25
-- Generated from EDMX file: C:\Users\ben.GOTHAM\Source\Repos\carpooling\CarPooling\CarpoolingModel.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [CarPoolingDB];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[FK_JourneyUser]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Journeys] DROP CONSTRAINT [FK_JourneyUser];
GO
IF OBJECT_ID(N'[dbo].[FK_ReservationJourney]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Reservations] DROP CONSTRAINT [FK_ReservationJourney];
GO
IF OBJECT_ID(N'[dbo].[FK_ReservationUser]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Reservations] DROP CONSTRAINT [FK_ReservationUser];
GO

-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[Journeys]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Journeys];
GO
IF OBJECT_ID(N'[dbo].[Reservations]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Reservations];
GO
IF OBJECT_ID(N'[dbo].[Users]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Users];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'Journeys'
CREATE TABLE [dbo].[Journeys] (
    [IdJourney] int IDENTITY(1,1) NOT NULL,
    [RefUser] int  NULL,
    [CityDeparture] nvarchar(max)  NOT NULL,
    [CityArrival] nvarchar(max)  NOT NULL,
    [Date] datetime  NOT NULL,
    [AvailableSeats] int  NOT NULL
);
GO

-- Creating table 'Reservations'
CREATE TABLE [dbo].[Reservations] (
    [IdReservation] int IDENTITY(1,1) NOT NULL,
    [RefUserId] int  NULL,
    [RefJourneyId] int  NOT NULL,
    [AcceptedByDriver] bit  NULL
);
GO

-- Creating table 'Users'
CREATE TABLE [dbo].[Users] (
    [IdUser] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NOT NULL,
    [DateBirth] datetime  NOT NULL,
    [FirstName] nvarchar(max)  NOT NULL,
    [Address] nvarchar(max)  NOT NULL,
    [City] nvarchar(max)  NOT NULL,
    [PostalNumber] int  NOT NULL,
    [PhoneNumber] nvarchar(max)  NOT NULL,
    [Username] nvarchar(max)  NOT NULL,
    [Password] nvarchar(max)  NOT NULL,
    [DateInscription] datetime  NOT NULL,
    [CarDescription] nvarchar(max)  NOT NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [IdJourney] in table 'Journeys'
ALTER TABLE [dbo].[Journeys]
ADD CONSTRAINT [PK_Journeys]
    PRIMARY KEY CLUSTERED ([IdJourney] ASC);
GO

-- Creating primary key on [IdReservation] in table 'Reservations'
ALTER TABLE [dbo].[Reservations]
ADD CONSTRAINT [PK_Reservations]
    PRIMARY KEY CLUSTERED ([IdReservation] ASC);
GO

-- Creating primary key on [IdUser] in table 'Users'
ALTER TABLE [dbo].[Users]
ADD CONSTRAINT [PK_Users]
    PRIMARY KEY CLUSTERED ([IdUser] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- Creating foreign key on [RefUser] in table 'Journeys'
ALTER TABLE [dbo].[Journeys]
ADD CONSTRAINT [FK_JourneyUser]
    FOREIGN KEY ([RefUser])
    REFERENCES [dbo].[Users]
        ([IdUser])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_JourneyUser'
CREATE INDEX [IX_FK_JourneyUser]
ON [dbo].[Journeys]
    ([RefUser]);
GO

-- Creating foreign key on [RefJourneyId] in table 'Reservations'
ALTER TABLE [dbo].[Reservations]
ADD CONSTRAINT [FK_ReservationJourney]
    FOREIGN KEY ([RefJourneyId])
    REFERENCES [dbo].[Journeys]
        ([IdJourney])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_ReservationJourney'
CREATE INDEX [IX_FK_ReservationJourney]
ON [dbo].[Reservations]
    ([RefJourneyId]);
GO

-- Creating foreign key on [RefUserId] in table 'Reservations'
ALTER TABLE [dbo].[Reservations]
ADD CONSTRAINT [FK_ReservationUser]
    FOREIGN KEY ([RefUserId])
    REFERENCES [dbo].[Users]
        ([IdUser])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_ReservationUser'
CREATE INDEX [IX_FK_ReservationUser]
ON [dbo].[Reservations]
    ([RefUserId]);
GO

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------