﻿using System;

namespace CarPooling
{
    /// <summary> Handling the data base to use by the application</summary>
    /// <remarks>Singleton</remarks>
    public class CurrentDataSource
    {
        /// <summary>Current instance</summary>
        private static CurrentDataSource instance = null;

        /// <summary>Use this method to have the current datasource</summary>
        /// <returns>Current instance or null if not found</returns>
        /// <remarks>
        /// Use the method ChangeDataSource() to modify the instance.
        /// If the instance is null the method try to create a new one according the config file
        /// </remarks>
        public static CurrentDataSource Instance()
        {
            if (instance == null)
            {
                ChangeDataSource();
            } 
            return instance;
        }
        /// <summary>Use this method to have the current datasource</summary>
        /// <returns>Current instance or null if not found</returns>
        /// <remarks>
        /// Use the method ChangeDataSource() to modify the instance.
        /// If the instance is null the method try to create a new one 
        /// according the argument. 
        /// If THE ARGUMENT is empty, the instance tries with the config file
        /// </remarks>
        /// <param name="pDataSourceToUse">String to define the instance.
        /// It's not sensible to case
        /// </param>
        public static CurrentDataSource Instance(String pDataSourceToUse)
        {
            if (instance == null)
            {
                ChangeDataSource(pDataSourceToUse);
            }
            return instance;
        }
        /// <summary>Reset the current instance by a new one</summary>
        /// <param name="pDataSourceToUse">optionnal parameter
        /// String to define the instance
        /// </param>
        public static void  ChangeDataSource(String pDataSourceToUse="")
        {
            //If nothing is specified by the argument, we try to use the info in the config file
            if (String.IsNullOrEmpty(pDataSourceToUse))
            {
                pDataSourceToUse = Properties.Settings.Default.DataSource;
                if (String.IsNullOrEmpty(pDataSourceToUse))
                {
                    pDataSourceToUse = DataSourceType.demo.ToString();
                    throw new NotImplementedException();
                    //Todo event bus
                }
            }

            pDataSourceToUse = pDataSourceToUse.ToLower();
            if(pDataSourceToUse== DataSourceType.demo.ToString())
            {
                currentDS = new DataSourceDemo();
            }
            else if(pDataSourceToUse == DataSourceType.database.ToString())
            {
                currentDS = new DatasourceMDF();               
            }
            else
            {
                //Todo event bus
                throw new NotImplementedException();
            }     
            
        }
        private static IDataSource currentDS = null;

        /// <summary>Return the current Data data base</summary>
        public static IDataSource CurrentDS {
            get {
                if (currentDS == null)
                {
                    ChangeDataSource();
                }
                return currentDS;
            }
        }
    }
}
