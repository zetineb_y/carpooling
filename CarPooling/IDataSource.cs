﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CarPooling;

namespace CarPooling
{
    /// <summary>
    /// Interface To connect to the database
    /// </summary>
   public interface IDataSource
    {
        List<Journey> getJourney();
        List<User> getUsers();
        User getUser(string pAccount);
        User getUser(int dataBaseId);
        FeedBack login(string pAccount, string pPassword);
        void addUser(User pUser);
        void addJourney(Journey pJourney);
        void modifyUser(User pUser);
        List<Reservation> getReservation();
        void participateJourney(Journey pJourney, int pUserId);
        void reservationForUser(int pUserId, int pJourneyId, bool pAccepted);
        int getAcceptedUsersNumber(int pJourneyId);
    }
}
