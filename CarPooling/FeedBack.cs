﻿using System;

namespace CarPooling
{
    /// <summary>
    /// Class that used to give a statut after an action and the ground if there is a problem.
    /// </summary>
    public class FeedBack
    {
        private Boolean success;
        private String errorCode;
        private String defaultMessage;

        /// <summary>
        /// Return true if the action has been played correctly
        /// </summary>
        public bool Success
        {
            get
            {
                return success;
            }

            set
            {
                success = value;
            }
        }

        /// <summary>
        /// A method that used this class can return a code error if an error is raised
        /// Therefore this code can be use for the log, for the language and GUI handling
        /// </summary>
        /// <remarks>The code have to unique in the application to profit of this function</remarks>
        public string ErrorCode
        {
            get
            {
                return errorCode;
            }

            set
            {
                errorCode = value;
            }
        }

        /// <summary>Provide a default message.</summary>
        /// <remarks>The language have to type in English</remarks>
        public string DefaultMessage
        {
            get
            {
                return defaultMessage;
            }

            set
            {
                defaultMessage = value;
            }
        }
    }
}
