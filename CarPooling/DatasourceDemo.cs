﻿using System;
using System.Collections.Generic;

namespace CarPooling
{
    /// <summary>
    /// Data source used to initialise the project before the database SQL is implemented
    /// data dummy
    /// </summary>
    /// <remarks>Normaly this on would be not used in production.
    /// it stays sin place if in the futur we want reused this one (demo, test unit and more)</remarks>
    ///<exception cref="NotImplementedException">This exception have to insert in new method develloped</exception>
    public class DataSourceDemo : DataSource
    {
        public override List<Journey> getJourney()
        {
            List<Journey> maListe = new List<Journey>();

            Journey monTrajet = new Journey();
            monTrajet.CityDeparture = "Bulle";
            monTrajet.CityArrival = "Lausanne";
            maListe.Add(monTrajet);
            monTrajet = null;

            monTrajet = new Journey();
            monTrajet.CityDeparture = "Lausanne";
            monTrajet.CityArrival = "Yverdon";
            maListe.Add(monTrajet);
            monTrajet = null;

            monTrajet = new Journey();
            monTrajet.CityDeparture = "Yverdon";
            monTrajet.CityArrival = "Avenches";
            maListe.Add(monTrajet);
            monTrajet = null;

            monTrajet = new Journey();
            monTrajet.CityDeparture = "Sion";
            monTrajet.CityArrival = "Genève";
            maListe.Add(monTrajet);
            monTrajet = null;


            return maListe;

        }

        public override List<User> getUsers()
        {
            List<User> mesUsers = new List<User>();
            User user1 = new User();
            user1.Name = "toto";
            user1.FirstName = "tata";
            user1.Address = "Rue d'Yverdon 2";
            user1.City = "Yverdon";
            user1.PostalNumber = 1400;
            user1.DateBirth = new DateTime(1980, 02, 29);
            user1.PhoneNumber = "0241111111";
            user1.DateInscription = new DateTime(2008, 12, 25);
            user1.Password = "1234";


            //user1.Profils.Add(user1Profil);

            mesUsers.Add(user1);

            User user2 = new User();
            user2.Username = "titi";
            user2.Name = "titi";
            user2.FirstName = "tutu";
            user2.Address = "Rue de Lausanne 20";
            user2.City = "Lausanne";
            user2.PostalNumber = 1004;
            user2.DateBirth = new DateTime(1990, 12, 2);
            user2.PhoneNumber = "0211234567";
            user2.DateInscription = new DateTime(2014, 05, 13);
            user2.Password = "1234";


            //user2.Profils.Add(user2Profil);

            mesUsers.Add(user2);

            return mesUsers;
        }

        public override User getUser(string pAccount)
        {
            User user1 = new User();
            user1.Username = pAccount;
            user1.Name = "toto";
            user1.FirstName = "tata";
            user1.Address = "Rue d'Yverdon 2";
            user1.City = "Yverdon";
            user1.PostalNumber = 1400;
            user1.DateBirth = new DateTime(1980, 02, 29);
            user1.PhoneNumber = "0241111111";
            user1.DateInscription = new DateTime(2008, 12, 25);
            user1.Password = "1234";
            user1.IdUser = 567;

            return user1;

        }

        public override FeedBack login(string pAccount, string pPassword)
        {
            FeedBack currentFeedback = null;
            if (pAccount == "toto")
            {
                if (pPassword == "1234")
                {
                    currentFeedback = isSuccess();
                }
                else
                {
                    currentFeedback = errorPasswordIsNotKnow();
                }
            }
            else
            {
                currentFeedback = errorAccountIsNotKnow();
            }
            return currentFeedback;
        }

        public override User getUser(int dataBaseId)
        {
            return getUser("toto");
        }

        public override void addUser(User pUser)
        {
            throw new NotImplementedException();
        }

        public override void addJourney(Journey pJourney)
        {
            throw new NotImplementedException();
        }

        public override void modifyUser(User pUser)
        {
            throw new NotImplementedException();
        }

        public override void participateJourney(Journey pJourney, int pUserId)
        {
            throw new NotImplementedException();
        }

        public override List<Reservation> getReservation()
        {
            throw new NotImplementedException();
        }

        public override void reservationForUser(int pUserId, int pJourneyId, bool pAccepted)
        {
            throw new NotImplementedException();
        }

        public override int getAcceptedUsersNumber(int pJourneyId)
        {
            throw new NotImplementedException();
        }
    }
}
