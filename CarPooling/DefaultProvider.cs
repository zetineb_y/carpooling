﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarPooling
{
    // Default provider wich returns the already chosen elements in the solution
    class DefaultProvider : IProvider
    {
        // Just returns un new DBEntities instance
        public DBEntities CreateConnection()
        {
            return new DBEntities();
        }
    }
}
