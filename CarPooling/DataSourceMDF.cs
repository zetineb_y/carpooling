﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace CarPooling
{
    /// <summary>
    /// Datasource developped to avoid to create an instance server
    /// We use a MDF file. 
    /// The sql could be reused in an instance MS-SQL directly.
    /// </summary>
    /// <remarks>Implement entity framework</remarks>
    public class DatasourceMDF : DataSource
    {

        public override  List<User> getUsers()
        {
            using (DBEntities ctx = new DBEntities())
            {
                var query = from allUsers in ctx.Users
                            select allUsers;
                List<User>result = query.ToList<User>();

                return result;
            }
        }
        public override User getUser(string pAccount)
        {
            using (DBEntities ctx = new DBEntities())
            {
                var query = from activeUser in ctx.Users
                            where activeUser.Username == pAccount
                            select activeUser;
                User result = query.FirstOrDefault<User>();

                return result;
            }
            //throw new NotImplementedException();
        }
        public override User getUser(int dataBaseId)
        {
            using (DBEntities ctx = new DBEntities())
            {
                return ctx.Users.Find(dataBaseId);
            }

            //throw new NotImplementedException();
        }
        public override FeedBack login(string pAccount, string pPassword)
        {
            //todo Crypter password
            FeedBack currentFeedback = null;

            using (DBEntities ctx = new DBEntities())
            {

                var query = from activeUser in ctx.Users
                             where activeUser.Username == pAccount
                             select activeUser;
                User result = query.FirstOrDefault<User>();

                if (result == null)
                {
                    currentFeedback = errorAccountIsNotKnow();
                }
                else if(result.Password != pPassword)
                {
                    currentFeedback = errorPasswordIsNotKnow();
                }
                else
                {
                    currentFeedback = isSuccess();
                }

                return currentFeedback;
            }

            //throw new NotImplementedException();
        }

        public override List<Journey> getJourney()
        {
            //todo select all journey

            using (DBEntities ctx = new DBEntities())
            {
                var query = from allJourney in ctx.Journeys
                            select allJourney;
                List<Journey> result = query.ToList<Journey>();

                return result;
            }

            //return new List<Journey>();
        }

        public override void addUser(User pUser)
        {
            using (DBEntities ctx = new DBEntities())
            {
                ctx.Users.Add(pUser);
                ctx.SaveChanges();
            }

            login(pUser.Username, pUser.Password);
        }

        public override void addJourney(Journey pJourney)
        {
            using (DBEntities ctx = new DBEntities())
            {
                ctx.Journeys.Add(pJourney);
                ctx.SaveChanges();
            }

            //throw new NotImplementedException();
        }

        public override void modifyUser(User pUser)
        {
            User userToUpdate;

            using (DBEntities ctx = new DBEntities())
            {
                userToUpdate = ctx.Users.Where(u => u.IdUser == pUser.IdUser).FirstOrDefault<User>();
            }

            if(userToUpdate != null)
            {
                userToUpdate.Name = pUser.Name;
                userToUpdate.FirstName = pUser.FirstName;
                userToUpdate.Address = pUser.Address;
                userToUpdate.City = pUser.City;
                userToUpdate.PostalNumber = pUser.PostalNumber;
                userToUpdate.DateBirth = pUser.DateBirth;
                userToUpdate.PhoneNumber = pUser.PhoneNumber;
                userToUpdate.CarDescription = pUser.CarDescription;
            }

            using (DBEntities dbCtx = new DBEntities())
            {
                dbCtx.Entry(userToUpdate).State = System.Data.Entity.EntityState.Modified;

                dbCtx.SaveChanges();
            }
        }

        public override void participateJourney(Journey pJourney, int pUserId)
        {
            Reservation resevationToDo = new Reservation();
            Journey journeyToUpdate;

            resevationToDo.RefUserId = pUserId;
            resevationToDo.RefJourneyId = pJourney.IdJourney;
            resevationToDo.AcceptedByDriver = null;

            using (DBEntities ctx = new DBEntities())
            {
                ctx.Reservations.Add(resevationToDo);
                ctx.SaveChanges();
                journeyToUpdate = ctx.Journeys.Where(j => j.IdJourney == pJourney.IdJourney).First<Journey>();
            }

            using (DBEntities dbCtx = new DBEntities())
            {
                dbCtx.Entry(journeyToUpdate).State = System.Data.Entity.EntityState.Modified;

                dbCtx.SaveChanges();
            }

            //throw new NotImplementedException();
        }

        public override List<Reservation> getReservation()
        {
            List<Reservation> res = null;
            using (DBEntities dbCtx = new DBEntities()) {
                var query = from allJourney in dbCtx.Reservations
                            select allJourney;
                res = query.ToList<Reservation>();
            }

            return res;
        }

        public override void reservationForUser(int pUserId, int pJourneyId, bool pAccepted)
        {
            using (DBEntities dbCtx = new DBEntities()) {
                var resQuery = from allReservation in dbCtx.Reservations
                               where allReservation.RefJourneyId == pJourneyId &&
                               allReservation.RefUserId == pUserId
                               select allReservation;
                Reservation toUpdate = resQuery.FirstOrDefault<Reservation>();

                if (pAccepted)
                {
                    toUpdate.AcceptedByDriver = true;
                }
                else {
                    toUpdate.AcceptedByDriver = false;
                }

                // update the record
                dbCtx.Entry(toUpdate).State = System.Data.Entity.EntityState.Modified;
                dbCtx.SaveChanges();
            }
        }

        public override int getAcceptedUsersNumber(int pJourneyId)
        {
            using (DBEntities dbCtx = new DBEntities())
            {

                var count = dbCtx.Reservations.Count(x => (x.RefJourneyId == pJourneyId) && (x.AcceptedByDriver == true));
                return count;
            }
        }
    }
}