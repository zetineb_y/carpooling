﻿using System.Collections.Generic;

namespace CarPooling
{
    /// <summary>Here are defined the generic method regaring the datasource</summary>
    public abstract class DataSource:IDataSource
    {

        public abstract List<Journey> getJourney();
        public abstract List<User> getUsers();
        public abstract User getUser(string pAccount);
        public abstract User getUser(int dataBaseId);
        public abstract FeedBack login(string pAccount, string pPassword);
        public abstract void addUser(User pUser);
        public abstract void addJourney(Journey pJourney);
        public abstract void modifyUser(User pUser);
        public abstract void participateJourney(Journey pJourney, int pUserId);
        public abstract List<Reservation> getReservation();
        public abstract void reservationForUser(int pUserId, int pJourneyId, bool pAccepted);
        public abstract int getAcceptedUsersNumber(int pJourneyId);
        /// <summary>
        /// Method that return a code error and a default message
        /// if the account is not identified in datasource
        /// </summary>
        /// <returns>the object feedback with the information regarging this error</returns>
        protected FeedBack errorAccountIsNotKnow()
        {
            FeedBack currentFeedback = new FeedBack();
            currentFeedback.Success = false;
            currentFeedback.ErrorCode = "rights001";
            currentFeedback.DefaultMessage = Properties.Resources.ErrorAccount;
            return currentFeedback;
        }
        /// <summary>
        /// Method that return a code error and a default message
        /// if the password is not identified in datasource
        /// </summary>
        /// <returns>the object feedback with the information regarging this error</returns>
        protected FeedBack errorPasswordIsNotKnow()
        {
            FeedBack currentFeedback = new FeedBack();
            currentFeedback.Success = false;
            currentFeedback.ErrorCode = "rights002";
            currentFeedback.DefaultMessage = Properties.Resources.ErrorPassword;
            return currentFeedback;
        }

        /// <summary> Method that return true if the actor is identified and authetified</summary>
        /// <returns>the object feedback with the statut succes true.
        /// No message are provide
        /// </returns>
        protected FeedBack isSuccess()
        {
            FeedBack currentFeedback = new FeedBack();
            currentFeedback.Success = true;
            currentFeedback.ErrorCode = "";
            currentFeedback.DefaultMessage = "";
            return currentFeedback;
        }
    }
}
