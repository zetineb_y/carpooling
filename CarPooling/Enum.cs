﻿namespace CarPooling
{ 
    /// <summary>
    /// Use to select the datasource to load
    /// </summary>
    /// <remarks>Normaly in production, database is used</remarks>
    public enum DataSourceType
    {
        demo,
        database,
        testunit
    }
}