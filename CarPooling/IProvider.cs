﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarPooling
{
    // Proposes an interface to define an objects provider. 
    // An interface has to be extended to satisfy some new needs
    
    // Interface which declares an object and which has a create entities method

    // any object can implement this 
    // object which provides a connexion
    
    public interface IProvider
    {
        // Exposes a method which creates a new connexion and returns it
        DBEntities CreateConnection();
    }
}
