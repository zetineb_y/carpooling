﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Data.Entity;
using CarPooling;
using System.Collections.Generic;

namespace UnitTestProjectCarPooling
{
    // DbSet = recordset, makes the link with the data base table

    // avoid using the BD --> mockUser = table de la BD
    public class MockUsers : DbSet<User>
    {
        public MockUsers()
        {
            TestUser = new User { IdUser = 3 };
            DataSet = new List<User>();
        }

        public List<User> DataSet { get; set; }


        public bool FindHasBeenCalled { get; private set; }
        public bool AddHasBeenCalled { get; private set; }
        public User TestUser { get; set; }

        // method can be called whith an objects table (objects with undefined types)
        // params allows a different writing convention, dive several users
        public override User Find(params object[] p)
        {
            FindHasBeenCalled = true;
            // is at table 0 position an int type and is it equal to 0
            if (p[0] is int && (int)p[0] == 3) return TestUser;
            return null;
        }

        public override User Add(User user)
        {
            AddHasBeenCalled = true;
            user.IdUser = 7;
            DataSet.Add(user);
            return user;
        }
    }

    [TestClass]
    public class UnitTest1
    {
        private MockUsers _users = null;
        private DBEntities _entities = null;
        private RelayProvider _provider = null;

        // method which doesn't take any parameter and which returns the entitiers field (getter)
        public DBEntities GetEntities() { return _entities; }

        [TestInitialize]
        public void Setup()
        {
            _users = new MockUsers();
            _entities = new DBEntities();
            _entities.Users = _users;

            _provider = new RelayProvider();
            _provider.ConnectionProvider = GetEntities;
        }

        [TestCleanup]

        public void TearDown()
        {
            _provider = null;
            _entities = null;
            _users = null;
        }

        [TestMethod]
        public void TestMethod1()
        {
            var dataSource = new DatasourceMDF
            {
                Provider = _provider
            };

            var user = dataSource.getUser(3);
            Assert.IsTrue(_users.FindHasBeenCalled, "the find method has not been called");
            Assert.IsNotNull(user, "is null but should have returned one");
        }

        [TestMethod]

        public void TestAdd()
        {
            var dataSource = new DatasourceMDF();
            dataSource.Provider = _provider;
            var user = new User();
            user.IdUser = 0;

            dataSource.addUser(user);
            Assert.IsTrue(_users.AddHasBeenCalled, "addMethod has not been called, while it should be");
            Assert.IsTrue(_users.DataSet.Contains(user), "User has not been inserted into DataSet");
        }
    }
}
