﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CarPooling;

namespace UnitTestProjectCarPooling.CarPoolingTest
{
    [TestClass]
    public class UnitTestCarPooling
    {

        /// <summary>
        /// Constructor
        /// </summary>        
        public UnitTestCarPooling()
        {
            CurrentDataSource database = CurrentDataSource.Instance(DataSourceType.demo.ToString());
            dataSource = CurrentDataSource.CurrentDS;
        }

        private IDataSource dataSource;

        // enable code collapsing
        #region Setup and TearDown 

        // Initialisation phases before doing the tests
        // Before beginning the tests, checks whether or not a method is decorated with AssemblyInitialize and calls it
        // Assembly is initialized and then before each test the initialize and cleanup are done
        // At the end the global text context is cleaned by a method decorated with [AssemblyCleanup] attribute


        [AssemblyInitialize]
        public static void AssemblySetup(TestContext context)
        {
            Console.WriteLine("AssemblySetup");
        }

        [AssemblyCleanup]
        public static void AssemblyTearDown()
        {
            Console.WriteLine("AssemblyTearDown");
        }

        [ClassInitialize]
        public static void ClassSetup(TestContext context) { }

        [ClassCleanup]
        public static void ClassTearDown() { }


        // Before each test a dataSourceDemo is created and at each test end it will be cleaned. 
        [TestInitialize]
        public void TestSetup()
        {
            dataSource = new DataSourceDemo();
            dataSource.addUser(new User() { Username = "titi1" });
            dataSource.addUser(new User() { Username = "titi2" });
            dataSource.addUser(new User() { Username = "titi3" });
        }

        // to delete, clean the environment test (in this case dataSourceDemo)
        [TestCleanup]
        public void TestTearDown()
        {
            // free ressources. Is my datasource a disposable ? If it is the case, the method is called. 
            
            var disposable = dataSource as IDisposable; // s'il arrive à le caster on aura un disposable, sinon on aura null
                                                        //disposable?.Dispose(); // s'il n'est pas null il appelle la méthode, sinon il ne l'appelle
                                                        // équivalent à disposable?.Dispose();
            if (disposable != null)
            {
                disposable.Dispose(); // dispose ressources de l'objet
            }

            dataSource = null;

        }

        #endregion


        /// <summary>
        /// Function which checks whether or not the connexion fails when a nonexistent username is provided
        /// </summary>
        /// <remarks>
        /// 
        /// </remarks>
        [TestMethod]
        public void WrongUsernameLoginTest()
        {
            FeedBack toCheck = dataSource.login("", "1234");
            Assert.IsFalse(toCheck.Success, "The evaluation regarding the missing account doesn't work");
            Assert.AreEqual("rights001", toCheck.ErrorCode);
        }

        /// <summary>
        /// Function which checks whether or not the connection fails when a wrong password is provided
        /// </summary>

        [TestMethod]
        public void WrongPasswordLoginTest()
        {
            FeedBack toCheck = dataSource.login("toto", "123456"); // mauvais mot de passe
            Assert.IsFalse(toCheck.Success, "The evaluation regarding the wrong password is not working properly");
            Assert.AreEqual("rights002", toCheck.ErrorCode, "Wrong error code");
        }

        /// <summary>
        /// Function which checks whether or not the connexion fails when a nonexistent username is provided or when a wrong password is provided
        /// </summary>

        [TestMethod]
        public void CorrectUsernameAndPasswordLoginTest()
        {
            FeedBack toCheck = dataSource.login("toto", "1234");
            Assert.IsTrue(toCheck.Success, "The evaluation regarding the right password is not working");
            Assert.AreEqual("", "", "The evaluation regarding the error code is not working");
        }

        /// <summary>
        /// Function which checks whether or not a user is correctly added into the users list
        /// 
        /// </summary>
        /// <remarks>
        /// 
        /// 
        /// </remarks>
        /// 

        [TestMethod]
        public void TestAddUserOk()
        {
            User u = new User() { Username = "titi" };

            User u2 = null;

            dataSource.addUser(u);
            u2 = dataSource.getUser("titi");

            Assert.IsNotNull(u2);
            Assert.AreEqual(u.Username, u2.Username);
        }

        /// <summary>
        /// 
        /// Function which checks whether or not a user is returned correctly by checking that the User type variable is not null and
        /// that the user exists in the database.
        /// 
        /// </summary>
        /// 
        [TestMethod]

        public void TestGetUserWhenExists()
        {
            User recup = dataSource.getUser("titi1");
            Assert.IsNotNull(recup, "No user given while a user should be given because the user exists");
            Assert.AreEqual("titi1", recup.Username, "The given user is not the right one");
        }

        /// <summary>
        /// Function which checks whether or not the function returns null when the user doesn't exist
        /// </summary>
        /// 
        [TestMethod]

        public void TestGetUserReturnsNullWhenUnexistingUser()
        {
            /* 
             * Randomly created string character and very likely non-existent as username
             * 
             */
            User u = dataSource.getUser(Guid.NewGuid().ToString());
            Assert.IsNull(u, "The username doesn't exist but a username is returned");
        }
    }
}


/*


[TestMethod]
public void TestExistenceVilleDepart()
{
    Journey testNomVilleDeDepart;
    testNomVilleDeDepart = new Journey();
    Assert.IsFalse(String.IsNullOrEmpty(testNomVilleDeDepart.CityDeparture), "Le champ ne peut pas etre vide");
}

[TestMethod]
public void testExistenceVilleArrivee()
{
    Journey testNomVilleArrivee;
    testNomVilleArrivee = new Journey();
    Assert.AreNotSame(testNomVilleArrivee.CityArrival, "", "Le champ ne peut pas etre vide");

}


[TestMethod]
public void testNbParticipantsPositif()
{
    Journey testNbParticipants2;
    testNbParticipants2 = new Journey();
    // Assert.IsTrue(testNbParticipants2.NbParticipant > 0, "Le nombre de participants ne peut etre nul ou inferieur a zero");
    /*
    if (testNbParticipants2. <= 0) // il faudrait vérifier pourquoi NbParticipant n'est pas un int
    {
        Console.WriteLine("Le nombre de participants ne peut etre nul ou inferieur a zero");
    }

}


[TestMethod]
public void testExistenceName()
{
    User testName;
    testName = new User();
    Assert.AreNotSame(testName.Name, "", "Le champ ne peut pas etre vide");
}

[TestMethod]
public void testExistenceFirstName()
{
    User testFirstName;
    testFirstName = new User();
    Assert.AreNotSame(testFirstName.FirstName, "", "Le champ ne peut pas etre vide");
}

[TestMethod]
public void testExistenceAddress()
{
    User testAdress;
    testAdress = new User();
    Assert.AreNotSame(testAdress.Address, "", "Le champ ne peut pas etre vide");
}

[TestMethod]
public void testExistenceCity()
{
    User testCity;
    testCity = new User();
    Assert.AreNotSame(testCity.City, "", "Le champ ne peut pas etre vide");
}



*/











